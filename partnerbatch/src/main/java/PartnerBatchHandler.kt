import com.google.gson.Gson
import com.google.gson.JsonObject
import za.co.hellogroup.persistence.DaoManager
import java.util.logging.Logger
import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 * Created by mark on 2017/06/10.
 */
@Path("/PartnerBatchHandler")
class PartnerBatchHandler {
    private val logger = Logger.getLogger("PARTNER BATCH HANDLER")
    /**
     * Entry point into the application
     * Casts the rawRequest into json
     * Processes the request
     * @return 0 - Updated Failed , 1 - Update Succeeded
     */
    @POST
    @Path(value = "/")
    fun processReq(@FormParam("BatchDetails") partnerBatchJson: String): Int {
        var updateResult = 0 //Default to update failed
        logger.info("[PARTNER BATCH HANDLER] Received request ::" + partnerBatchJson)
        val jsonObject = Gson().fromJson(partnerBatchJson, JsonObject::class.java)
        if (validateRequest(jsonObject)) {
            updateResult = BatchStatusUpdater().updateBatchStatus(Integer.valueOf(jsonObject.get("status").toString())!!, jsonObject.get("transactions").toString())
        }
        return updateResult
    }

    /**
     * Validate the raw json request and throws a null pointer if invalid
     * @param rawRequest - Raw json request received from the transaction
     * processor interface
     * Once validation is done, the @fun authenticateLoginDetails is called
     * and the @return result is passed back to the processReq fun
     */
    fun validateRequest(rawRequest: JsonObject): Boolean {
        if (rawRequest.get("username") != null && rawRequest.get("password") != null
                && rawRequest.get("status") != null && rawRequest.get("transactions") != null) {
            logger.info("[PARTNER BATCH HANDLER] Request is valid proceeding to authenticate user")
            return authenticateLoginDetails(rawRequest.get("username").toString(), rawRequest.get("password").toString())
        } else {
            throw NullPointerException("[PARTNER BATCH HANDLER] Invalid Request Specified")
        }
    }

    /**
     * Authenticate login details
     * @param userName - Internal auth
     * @param password - Internal auth
     * @return loginResult - Boolean - Default to false
     * unless auth was successful.
     */
    fun authenticateLoginDetails(userName: String, password: String): Boolean {
        var loginResult = false // Default to failure unless login details are correct
        logger.info("[PARTNER BATCH HANDLER] : Authenticating Login Details")
        if (!DaoManager.getDcmApiAuthenticationDao().authenticateUser(userName.replace("\"","").replace("'",""), password.replace("\"","").replace("'",""))) {
            logger.info("[PARTNER BATCH HANDLER] : Invalid Login Details")
        } else {
            logger.info("[PARTNER BATCH HANDLER] : Authentication Succeeded")
            loginResult = true
        }
        return loginResult
    }
}
