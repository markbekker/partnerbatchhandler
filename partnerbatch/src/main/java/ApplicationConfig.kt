import javax.ws.rs.core.Application

@javax.ws.rs.ApplicationPath("/*")
class ApplicationConfig : Application() {

    /**
     * Add class to hash set
     */
    override fun getClasses(): Set<Class<*>> {
        val resources = java.util.HashSet<Class<*>>()
        addRestResourceClasses(resources)
        return resources
    }

    private fun addRestResourceClasses(resources: MutableSet<Class<*>>) {
        resources.add(PartnerBatchHandler::class.java)
    }
}
