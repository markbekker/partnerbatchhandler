import za.co.hellogroup.common.ConfigurationHandler
import za.co.hellogroup.common.PPMailer
import za.co.hellogroup.hellopaisa.partners.common.model.DcmToHPMapper
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.SQLException
import java.util.logging.Logger

/**
 * Created by mark on 2017/06/13.
 */
open class BatchStatusUpdater {
    private val logger = Logger.getLogger(BatchStatusUpdater::class.java.simpleName)
    private val dbUrl = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "DBHostHelloPaisa")
    private val dbHostPartner = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "DBName")
    private val dbUser = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "DBUser")
    private val dbPassword = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "DBPassword")
    private val dbTable = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "BATCH_TABLE_NAME")
    private val dbUpdateColumn = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "BATCH_UPDATE_COLUMN")
    private val dbIdentifierColumn = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "BATCH_IDENTIFIER_COLUMN")
    private val dbPath = "jdbc:mysql://$dbUrl/$dbHostPartner"
    private val emailRecipients = ConfigurationHandler.getConfiguration("hellopaisa/partner_configuration", "PARTNER_BATCH_EMAIL")
    val conn = DriverManager.getConnection(dbPath, dbUser, dbPassword)!!
    var dcmErrorCode: String = ""

    /**
     * Creates an update query using config values and updates the
     * @param status of
     * @param refNumbers transactions on Dcm
     * After each request, an email is sent out confirming the
     * action performed and the exectution status thereof
     * @return 0 - Prepared Statement execution failure : 1 - Prepared Statement Successful Execution
     */
    fun updateBatchStatus(status: Int, refNumbers: String): Int {
        var stmt: PreparedStatement? = null
        var result = 0 //Defaults to failure unless execution was successful
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance()
            val updateQuery = "UPDATE $dbTable SET $dbUpdateColumn = ? WHERE $dbIdentifierColumn = ?"
            dcmErrorCode = DcmToHPMapper().mapHpErrorCodetoDcm(Integer.valueOf(status)).toString() //Converts the hp status code to dcm
            val refNumberArray = refNumbers.split(",")
            for (ref in refNumberArray) {
                val refNumber = ref.replace("\"", "").replace("'", "")
                print("Ref Number :: $refNumber")
                conn.autoCommit = false
                stmt = conn.prepareStatement(updateQuery)
                stmt.setString(1, dcmErrorCode)
                stmt.setString(2, refNumber)
                result = stmt.executeUpdate()
                if (result == 0) {
                    val updateFailed = "Failed to update status"
                    logger.info("[PARTNER BATCH HANDLER] : $updateFailed :: $dcmErrorCode for ref number(s) :: $refNumber")
                    PPMailer.sendEmail(emailRecipients, "Partner Batch Handler - $updateFailed", "$updateFailed :: $dcmErrorCode for ref number(s) :: $refNumber")
                } else {
                    val updateSuccess = "Successfully updated status"
                    logger.info("[PARTNER BATCH HANDLER] : $updateSuccess :: $dcmErrorCode for ref number(s) :: $refNumber")
                }
            }
            conn.commit()
        } catch (e: Exception) {
            e.printStackTrace()
            conn.rollback()
            conn.close()
        } finally { //Always close connection
            if (stmt != null) {
                try {
                    stmt.close()
                    val updateSuccess = "Successfully updated status"
                    PPMailer.sendEmail(emailRecipients, "Partner Batch Handler - $updateSuccess", "$updateSuccess :: $dcmErrorCode for ref number(s) :: ${refNumbers.replace("\"", "").replace("'", "")}")
                } catch (e: SQLException) {
                    e.printStackTrace()
                }
            }
            conn.autoCommit = true
            conn.close()
        }
        return result
    }
}
